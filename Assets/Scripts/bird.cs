using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class bird : MonoBehaviour
{

    private int framesLeftMovingUp = 0;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(3,0,0) * Time.deltaTime);

        if (framesLeftMovingUp > 0) {
            framesLeftMovingUp -= 1;
            transform.Translate(new Vector3(0,5,0) * Time.deltaTime);
        }
        else {
            transform.Translate(new Vector3(0,-7,0) * Time.deltaTime);
        }

        if (Input.GetKeyDown("space"))
        {
            framesLeftMovingUp = 50;
        }

        float ball_heigh = transform.position[1];
        if (ball_heigh > 5 || ball_heigh < -5){
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
