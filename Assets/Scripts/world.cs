using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class world : MonoBehaviour
{

    private int frames_till_spawn = 300;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (frames_till_spawn == 0){
            var pos = GameObject.Find("birdy").transform.position;

            int[] heights = {-3, -1, 1, 3,};
            int to_skip = heights[Random.Range(0,heights.Length)];
            for (int i = -5; i <= 5; i+=2){
                if (!( i == to_skip)){
                    GameObject bar = Instantiate(GameObject.Find("bar"));
                    bar.transform.position = new Vector3(pos[0] + 10, i, 0);
                }
            }

            frames_till_spawn = 300;
        }else{
            frames_till_spawn -= 1;
        }
    }
}
